package utility;

import java.io.PrintStream;
import java.util.function.Predicate;

public class Utility {
    public static void printIf(String[] arr, PrintStream outputStream, Predicate<String> predicate) {
        for (var s : arr) {
            if (predicate.test(s)) {
                outputStream.println(s);
            }
        }
    }
}
