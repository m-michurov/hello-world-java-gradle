import java.io.PrintStream;

public class Application {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        System.out.println("My args are:");

        printArguments(args, System.out);
    }

    public static void printArguments(String[] args, PrintStream outputStream) {
        for (var s : args) {
            outputStream.println(s);
        }
    }
}
