package utility;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class UtilityTest {

    @Test
    void printIf() {
        var byteArrayOS = new ByteArrayOutputStream();
        var testStream = new PrintStream(byteArrayOS);

        Utility.printIf(new String[]{"short", "very long", "moderate"}, testStream, s -> s.length() > 5);

        assertEquals("very long\nmoderate\n", byteArrayOS.toString());

        byteArrayOS.reset();

        Utility.printIf(new String[]{"", "string", ""}, testStream, s -> !s.isEmpty());

        assertEquals("string\n", byteArrayOS.toString());
    }
}