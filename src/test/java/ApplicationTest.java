import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class ApplicationTest {

    @Test
    void printArguments() {
        var byteArrayOS = new ByteArrayOutputStream();
        var testStream = new PrintStream(byteArrayOS);

        Application.printArguments(new String[] {"arg1", "arg2 arg3", "", "arg4"}, testStream);

        assertEquals(byteArrayOS.toString(), "arg1\narg2 arg3\n\narg4\n");
    }
}